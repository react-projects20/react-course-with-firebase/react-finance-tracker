import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { useAuthContext } from "./hooks/useAuthContext";

//Pages & Components
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import Signup from "./pages/signup/Signup";
import Navbar from "./components/Navbar"

function App() {
  //I want to use authIsReady from the hook called useAuthContext and set that to the useAuthContext() function

  const { authIsReady, user } = useAuthContext()

  return (
    <div className="App">
      {authIsReady && (
        <BrowserRouter>
          <Navbar></Navbar>
          <Switch>
            <Route exact path="/">
              {user && <Home />}
              {!user && <Redirect to="/login"/>}
            </Route>

            <Route exact path="/login">
              {!user && <Login />}
              {user && <Redirect to="/"/>}
            </Route>

            <Route exact path="/signup">
              {!user && <Signup />}
              {user && <Redirect to="/"/>}
            </Route>

          </Switch>
        </BrowserRouter>
      )}
    </div>
  );
}

export default App
