import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyDj8l4bb_L8Ne6vZFVQmde8N5sh6NGIWy8",
    authDomain: "mymoney-982ed.firebaseapp.com",
    projectId: "mymoney-982ed",
    storageBucket: "mymoney-982ed.appspot.com",
    messagingSenderId: "1039207708637",
    appId: "1:1039207708637:web:a70782b7deb857665f6834"
  };

  // intit firebase
  firebase.initializeApp(firebaseConfig)

  // init service
  const projectFirestore = firebase.firestore()
  const projectAuth = firebase.auth()

  // timestamp
  const timestamp = firebase.firestore.Timestamp

  export {projectFirestore, projectAuth, timestamp}