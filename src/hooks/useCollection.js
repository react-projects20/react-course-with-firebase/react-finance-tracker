import { useEffect, useRef, useState } from "react"
import { projectFirestore } from "../firebase/config"

export const useCollection = (collection, _query, _orderby) => {
  const [documents, setDocuments] = useState(null)
  const [error, setError] = useState(null)

  //If we dont user a ref --> an infinte loop is now in useEffect
  //_query is an array and is different on every function call
  const query = useRef(_query).current

  //This is going to order the transactions by descending order
  const orderBy = useRef(_orderby).current

  useEffect(() => {
    let ref = projectFirestore.collection(collection)

    if (query) {
      ref = ref.where(...query)
    }

    if (orderBy) {
      ref = ref.orderBy(..._orderby)
    }

    const unsubscribe = ref.onSnapshot(snapshot => {
      let results = []
      snapshot.docs.forEach(doc => {
        console.log(doc)
        results.push({...doc.data(), id: doc.id})
      });
      
      // update state
      setDocuments(results)
      setError(null)
    }, error => {
      console.log(error)
      setError('could not fetch the data')
    })

    // unsubscribe on unmount
    return () => unsubscribe()

  }, [collection, query])

  return { documents, error }
}